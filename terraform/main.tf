terraform {
  backend "swift" {
    container         = "terraform-state-terraform-demo"
    archive_container = "terraform-state-terraform-demo-archive"
  }
}

resource "openstack_compute_keypair_v2" "terraform-demo-key" {
  name       = "terraform-demo-key"
  public_key = file("${var.ssh_key_file}.pub")
}

resource "openstack_networking_network_v2" "terraform-demo-net" {
  name           = "terraform-demo-net"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "terraform-demo-subnet" {
  name            = "terraform-demo-subnet"
  network_id      = openstack_networking_network_v2.terraform-demo-net.id
  cidr            = "192.168.0.0/16"
  ip_version      = 4
  dns_nameservers = ["147.251.4.33", "147.251.6.10", "8.8.8.8"]
}

data "openstack_networking_network_v2" "terraform-demo-external-net" {
  name = var.pool
}

resource "openstack_networking_floatingip_v2" "terraform-demo-fip" {
  pool = var.pool
}

resource "openstack_networking_router_v2" "terraform-demo-router" {
  name                = "terraform-demo-router"
  admin_state_up      = "true"
  external_network_id = data.openstack_networking_network_v2.terraform-demo-external-net.id
}

resource "openstack_networking_router_interface_v2" "terraform-demo-router-interface-1" {
  router_id = openstack_networking_router_v2.terraform-demo-router.id
  subnet_id = openstack_networking_subnet_v2.terraform-demo-subnet.id
}

resource "openstack_compute_instance_v2" "terraform-demo-vm" {
  name            = "terraform-demo"
  image_id        = var.image
  flavor_name     = var.flavor
  key_pair        = openstack_compute_keypair_v2.terraform-demo-key.name
  security_groups = [openstack_networking_secgroup_v2.terraform-demo-secgroup.name]
  user_data       = "#cloud-config\nhostname: terraform.demo\nfqdn: terraform.demo"

  network {
    uuid = openstack_networking_network_v2.terraform-demo-net.id
  }

}

resource "openstack_networking_secgroup_v2" "terraform-demo-secgroup" {
  name        = "terraform-demo-secgroup"
  description = "Security group for OpenStack Terraform Demo"
}

resource "openstack_networking_secgroup_rule_v2" "https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.terraform-demo-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.terraform-demo-secgroup.id
}

resource "openstack_networking_secgroup_rule_v2" "ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.terraform-demo-secgroup.id
}

resource "openstack_compute_floatingip_associate_v2" "fip_1" {
  floating_ip = openstack_networking_floatingip_v2.terraform-demo-fip.address
  instance_id = openstack_compute_instance_v2.terraform-demo-vm.id

  provisioner "local-exec" {
    command = "echo ${openstack_networking_floatingip_v2.terraform-demo-fip.address} >> ../ansible/ansible_hosts"
  }

  provisioner "remote-exec" {
    connection {
      host        = openstack_networking_floatingip_v2.terraform-demo-fip.address
      user        = var.ssh_user_name
      private_key = file(var.ssh_key_file)
    }

    inline = [
      "sudo dnf -y install wget",
    ]
  }
}
