variable "ssh_key_file" {
  default = "~/.ssh/id_rsa"
}

variable "image" {
  default = "1196e919-0337-4ba9-8346-5bc298170816"
}

variable "flavor" {
  default = "standard.small"
}

variable "pool" {
  default = "public-cesnet-78-128-251-GROUP"
}

variable "public_key" {
    type = string
    description = "public key"
}

variable "ssh_user_name" {
  default = "centos"
}
