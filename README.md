# Terrafrom Demo

At this repo can be found an example of creating basic cloud infrastructure with Terraform and starting web server with ansible

## How to deploy
* Create [applicatin credentials](https://cloud.gitlab-pages.ics.muni.cz/documentation/cloud/cli/#getting-credentials) for your project and copy them to Terrform catalog
* Initialise your Terraform  backend with `terraform init`
* Run Terraform with `terraform apply`

Terraform will pripare network infrastructure and create a VM. Then ansible will install web server with basic configuration
* go to ansible catalog
* run `ansible-playbook install_web_server.yml -i ansible_hosts`
* try to open web page 
